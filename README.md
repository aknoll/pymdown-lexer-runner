# pymdown-lexers for RuNNer

A package created to add additional lexers for use in Pygments. This package was created to be used in the [RuNNer documentation](https://gitlab.gwgd.de/aknoll/runner-clean).

## Overview

This package was created in order to add RuNNer syntax highlighting to its documentation.

## Included Lexers

Lexers       | Description
------------ |------------
runner-data | A simple lexer used in PyMdown documents to highlight RuNNer input.data files
runner-config | Lexer for RuNNer input.nn files

## Acknowledgements
This package was created based on the [template by facelessuser](https://github.com/facelessuser/pymdown-lexers).