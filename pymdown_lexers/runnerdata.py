"""Pygments lexer for RuNNer input.data files."""
from pygments.lexer import RegexLexer
from pygments.token import *

__all__ = ("RuNNerDataLexer",)

class RuNNerDataLexer(RegexLexer):
    """Simple lexer for RuNNer input.data files.
    
    This class provides syntax highlighting based on the keywords that appear
    in RuNNer input.data files. 
    
    Parameters:
    -----------
    - RegexLexer : Extends the regular expression lexer base class of Pygments.
    
    Provides:
    ---------
    - tokens : A dictionary referencing the style label of required keywords. 
     
    """
    
    # Define how the lexer can be discovered in markdown documents.
    name = 'RuNNerData'
    aliases = ['runner-data'] # The label at the beginning of .md code blocks. 
    filenames = ['*.data'] # All files ending in .nn will use this style.
    mimetypes = ['text/runner-data']

    # Define the keywords and regular expression and their respective styles.
    tokens = {
        'root': [
            (r'^begin', String),
            (r'^end', String),
            (r'^comment', String),
            (r'^lattice', String),
            (r'^atom', String),
            (r'^energy', String),
            (r'^charge', String)                                        
        ]
    }
